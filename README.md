# Análises da Copa do Mundo 

# ANÁLISE 1 - Gráfico de frequência sobre as posições de cada seleção nas copas do mundo

# Bibliotecas ----
pacman::p_load(tidyverse, readxl, lubridate)

# Definindo as cores da estat ---
cores_estat <- c('#A11D21','#663333','#FF6600','#CC9900','#CC9966', 
                 '#999966','#006606','#008091','#003366','#041835','#666666')

# Definindo o tema da estat ---
theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

Jogos <- read_excel("World_Cups.xlsx",  #Criando conjunto de dados dos Jogos
                       sheet = "Partidas")
Continentes <- read_excel("Continentes.xlsx") #Criando conjunto de dados dos continentes
World_Cups <- read_excel("World_Cups.xlsx", #Criando conjunto de dados gerais das copas
                         sheet = "Copas")




# Quantidade de gols em uma edição de copa ----

gols_copa<- World_Cups[c(1,7)]

ggplot(gols_copa,aes(x = Year,y=GoalsScored, label=GoalsScored))+
  geom_bar(stat = 'identity', position = 'dodge', fill='#A11D21')+ # dodge ou stack
  geom_text(position = position_dodge(width = .9), vjust = -0.5, hjust = 0.5, size = 3)+
  labs(x='Países', y='Nº de vezes na posição')+
  theme_bw()+
  theme(axis.title.y = element_text(colour = 'black', size=12),
        axis.title.x = element_text(colour = 'black', size=12),
        axis.text = element_text(colour = 'black', size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = 'black'))+
  theme(legend.position = 'top')

ggplot(gols_copa) +
  aes(x = Year, y = GoalsScored, label = GoalsScored) + 
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) + 
  scale_x_continuous(breaks = seq(1930,2014,8))+
  scale_y_continuous(breaks = seq(0,180,25))+
  geom_text(
    position = position_dodge(width = .9), vjust = -0.5, #hjust = .5,
    size = 3
  )+
  labs(x = "Ano da edição", y = "Gols") + 
  theme_estat()
ggsave("gols_edicao.pdf", width = 158, height = 93, units = "mm" )



# Transformando a Alemanha Oriental em Alemanha ----
World_Cups$Winner<-World_Cups$Winner %>% str_replace('Germany FR', 'Germany')
World_Cups$`Runners-Up`<-World_Cups$`Runners-Up` %>% str_replace('Germany FR', 'Germany')
World_Cups$Third<-World_Cups$Third %>% str_replace('Germany FR', 'Germany')

# Criando um gráfico de barras com a frequencia de campeões e vice campeões ----

campeoes<-data.frame(paises=World_Cups$Winner) # Pegando os campeões 
campeoes$posicao<-c('Campeão') # Adicionando coluna de campeão

vicecampeoes<-data.frame(paises = World_Cups$`Runners-Up`)%>%
  filter(paises %in% campeoes$paises)
vicecampeoes$posicao<-c('Vice') # Adicionando coluna de vice

Camp_vices <- bind_rows(campeoes, vicecampeoes)
Camp_vices<- Camp_vices%>% # juntando os dois DF
  group_by(paises, posicao) %>%
  summarise(freq=n())%>%
  arrange(desc(freq))%>%
  mutate(freq_relat = round((freq/sum(freq)),2))

porcentagens <- str_c(Camp_vices$freq_relat, "%") %>% str_replace(" \\.", ",")
legendas <- str_squish(str_c(Camp_vices$freq))


ggplot(Camp_vices,aes(x = fct_reorder(paises, freq, .desc = T),y=freq, fill=posicao, label=legendas))+
  geom_bar(stat = 'identity', position = 'dodge')+ # dodge ou stack
  scale_fill_manual(name='Posição', values = c('#A11D21', '#003366'))+
  geom_text(position = position_dodge(width = .9), vjust = -0.5, hjust = 0.5, size = 3)+
  labs(x='Países', y='Nº de vezes na posição')+
  theme_bw()+
  theme(axis.title.y = element_text(colour = 'black', size=12),
        axis.title.x = element_text(colour = 'black', size=12),
        axis.text = element_text(colour = 'black', size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = 'black'))+
  theme(legend.position = 'top')

ggsave("campeao_vice_colunas.pdf", width = 158, height = 93, units = "mm")


# Público por edição da copa ----
ggplot(World_Cups) +
  aes(x=Year, y=Attendance/(10^6), group=1) +
  geom_line(size=1,colour="#A11D21") + geom_point(colour="#A11D21",
                                                  size=2) +
  labs(x="Ano da Edição", y="Público (em milhões de pessoas)") +
  scale_y_continuous(breaks = seq(0,4,0.25))+
  scale_x_continuous(breaks = seq(1930,2014,8))+
  theme_estat()
ggsave("serie_publico.pdf", width = 158, height = 93, units = "mm")

porcentagens <- str_c(trans_drv$freq_relativa, "%") %>% str_replace("\\.", ",")

legendas <- str_squish(str_c(trans_drv$freq, " (", porcentagens, ")"))

ggplot(trans_drv) +
  aes(
    x = fct_reorder(trans, freq, .desc = T), y = freq,
    fill = drv, label = legendas
  ) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = 0.5,
    size = 3
  ) +
  labs(x = "Transmissão", y = "Frequência") +
  theme_estat()
ggsave("colunas-bi-freq.pdf", width = 158, height = 93, units = "mm")

# ANÁLISE 2 - Relação entre número de gols marcados por partida e continente do time da casa (home team)

# Instalando pacotes

library(readxl)
library(ggplot2)
library(dplyr)
library(tidyverse)
library(stringr)

# Adcionando os Bancos de Dados
Continentes<-read_excel('Continentes.xlsx')
World_Cups <- read_excel("World_Cups.xlsx", 
                         sheet = "Copas")
Jogos <- read_excel("World_Cups.xlsx")

Jogos<-Jogos%>%
  distinct(Id=Jogos$MatchID, .keep_all = T)

# Padronização da ESTAT
cores_estat<-c( "#A11D21", "#003366", "#663333", "#FF6600", "#CC9900",
          "#CC9966", "#999966", "#006606",
           "#008091", "#041835", "#666666")

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

# Quantidade de gols feitos pelo time da casa

golCasa<-data.frame()
for (i in 1:836) {
  x<-subset(Jogos, Jogos$`Home Team Name`==Jogos$`Home Team Name`[i])
  x<-x%>%
    mutate(gols_casa = sum(x$`Home Team Goals`))%>%
    mutate(media_gol_casa = round((sum(x$`Home Team Goals`)/count(x)),
                                 digits = 2))%>%
    mutate(jogos=count(x))
  golCasa<-bind_rows(golCasa,x[1,])
}

Continentes2<-Continentes%>%     #Reconhecendo os países e adcionando uma coluna com os continentes
  filter(FIFA %in% golCasa$`Home Team Initials`)
golCasaCont<-data.frame()
for (i in 1:78){
  x<-subset(golCasa, golCasa$`Home Team Initials`==Continentes2$FIFA[i])%>%
    mutate(continente=Continentes2$Continent[i])
  golCasaCont<-bind_rows(golCasaCont,x)
}
golCasaCont<-golCasaCont%>%
  distinct(golCasaCont$MatchID, .keep_all = T)
golCasaCont<-golCasaCont[,-c(20,25)]


# Substituindo os nomes dos continentes 

golCasaCont$continente<-golCasaCont$continente%>%
  str_replace('AF','África')%>%
  str_replace('AS','Ásia')%>%
  str_replace('SA','América do S.')%>%
  str_replace('OC','Oceania')%>%
  str_replace('EU','Europa')%>%
  str_replace('NA','América do N.')


golCasaCont<-golCasaCont%>%
  filter(!media_gol_casa$n==0.00)
golCasaCont0_1$`Home Team Name`<-golCasaCont0_1$`Home Team Name`%>%
  str_replace('rn">Republic of Ireland', 'Ireland')

# Gráfico dispersão gols como mandante

ggplot(golCasaCont) +
  aes(y=`Home Team Initials`, x=gols_casa) +
  geom_point(aes(colour=continente)) +
  labs(
    y="Equipe mandante", x="Gols", colour='Continente'
  ) +
  theme_estat(axis.text.y = element_blank(),
              axis.ticks.y = element_blank())
ggsave("disp_gol_mandante.pdf", width = 158, height = 93, units = "mm")

# Boxplot continentes por gol

ggplot(golCasaCont) +
  aes(x = continente, y = gols_casa) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
  ) +
  labs(x = "Continente", y = "Gols marcados pelas equipes mandantes") +
  theme_estat()
ggsave("box_bi_continente_gol_casa.pdf", width = 158, height = 93, units = "mm")

# Criando um histograma com a quantidade de gols marcados

tabela<-golCasaCont%>%
  group_by(`Home Team Goals`)%>%
  summarise(freq_gol=n()) # Frequência de vezes que a quantidade de gols ocorreu

tabela$freq<-cut(tabela$`Home Team Goals`,
                 breaks = seq(-1,7,1),
                 labels = c('0 Gols', '1 Gols', '2 Gols', '3 Gols', '4 Gols',
                            '5 Gols', '6 Gols', '7 Gols'))

# Gráfico de colunas contando frequência de gols a equipe mandante

ggplot(tabela,aes(x=freq, y = freq_gol, label = freq_gol)) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3
  ) +
  labs(x = "Quantidade de Gols", y = "Frequência") +
  theme_estat()
ggsave("colunas-gols-casa-freq.pdf", width = 158, height = 93, units = "mm")

# Gráfico de disperção de média de gols por continente

ggplot(golCasaCont) +
  aes(y=media_gol_casa$n, x=continente) +
  geom_point(aes(colour=continente)) +
  labs(
    y="Equipe mandante", x="Média de gols por partida", colour='Continente'
  ) +
  theme_estat()
ggsave("disp_gol_mandante_cont.pdf", width = 158, height = 93, units = "mm")

# ANÁLISE 3 - Relação entre número de partidas por etapa (fase de grupos x eliminatórias) e turno da partida (diurno ou noturno)

# Padronizando as fases 

Jogos$Stage<-Jogos$Stage%>%
  str_replace('Play-off for third place|Match for third place','Third place')%>%
  str_replace('Group [1-8]|Group [A-J]','Grupo')
  
# Gráfico de barras sobre a frequência das fases

elimi_grupo <- Jogos
elimi_grupo$Stage<-elimi_grupo$Stage%>%  #Padronizando as fases
  str_replace('First round|Preliminary round','Outros')%>%
  str_replace('Third place','Terceiro Lugar')%>%
  str_replace('Semi-finals','Semi')%>%
  str_replace('Round of 16','Oitavas')%>%
  str_replace('Quarter-finals','Quartas')

elimi_grupo <- elimi_grupo%>% #Criando frequência das fases
  filter(!is.na(Stage)) %>%
  count(Stage) %>%
  mutate(
    freq=n(), freq_relat=round((n/sum(n))*100,2)) %>%
  mutate(
    freq = gsub("\\.", ",", freq_relat) %>% paste("%", sep = ""),
    label = str_c(n, " (", freq, ")") %>% str_squish()
  )

ggplot(elimi_grupo) + #Criando o gráfico das fases
  aes(x = fct_reorder(Stage, n, .desc=T), y = n, label = label) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  scale_y_continuous(limits = c(0,650))+
  labs(x = "Fase disputada", y = "Frequência") +
  theme_estat()
ggsave("colunas-fase-disputadas-freq.pdf", width = 158, height = 93, units = "mm")

# Edicao, jogos e fases ----
edicao_fase<-Jogos
edicao_fase$Datetime<-str_extract(Jogos$Datetime, '[0-9]{4}')
edicao_fase$Stage<-edicao_fase$Stage%>%
  str_replace('First round|Preliminary round|Semi-finals|Round of 16|Play-off for third place|Match for third place|Final|Third place|Quarter-finals',
              'Eliminatórias')%>%
  str_replace('Group [1-8]|Group [A-J]','Grupo')

edi_jogo_fase <- edicao_fase  %>%
  group_by(Datetime,Stage) %>%
  summarise(freq = n()) %>%
  mutate(
    freq_relativa = round(freq/sum(freq)*100,2)
  )

porcentagens <- str_c(edi_jogo_fase$freq_relativa, "%") %>% str_replace("\\.", ",")

legendas <- str_squish(str_c(edi_jogo_fase$freq))

ggplot(edi_jogo_fase) +
  aes(
    x = Datetime, y = freq,
    fill = Stage, label = legendas
  ) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = 0.5,
    size = 3
  ) +
  labs(x = "Edição", y = "Frequência", fill='Fase') +
  theme_estat()
ggsave("colunas-bi-freq-freq-edicao-fase.pdf", width = 158, height = 93, units = "mm")

# Jogos por turno ----
edicao_fase<-Jogos
edicao_fase$Datetime<-str_extract(Jogos$Datetime, '[0-9]{2}:[0-9]{2}')
edicao_fase$Stage<-edicao_fase$Stage%>%
  str_replace('First round|Preliminary round|Semi-finals|Round of 16|Play-off for third place|Match for third place|Final|Third place|Quarter-finals',
              'Eliminatórias')%>%
  str_replace('Group [1-8]|Group [A-J]','Grupo')

noite<-filter(edicao_fase, Datetime >= hms("18H 0M 0S"))
noite['turno']<-c('Noite')
tarde<-filter(edicao_fase, Datetime >= hms("12H 0M 0S"), Datetime<hms("18H 0M 0S"))
tarde['turno']<-c('Dia')
manha<-filter(edicao_fase, Datetime>=hms("06H 0M 0S"), Datetime<hms("12H 0M 0S"))
manha['turno']<-c('Dia')
turnos_jogos<-bind_rows(manha,tarde,noite)

contagem <- turnos_jogos %>% 
  group_by(turno) %>%
  summarise(Freq = n()) %>%
  mutate(Prop = round(100*(Freq/sum(Freq)), 2)) %>%
  arrange(desc(turno)) %>%
  mutate(posicao = cumsum(Prop) - 0.5*Prop)

ggplot(contagem) +
  aes(x = factor(""), y = Prop , fill = factor(turno)) +
  geom_bar(width = 1, stat = "identity") +
  coord_polar(theta = "y") +
  geom_text(
    aes(x = 1.8, y = posicao, label = paste0(Prop, "%")),
    color = "black"
  ) +
  theme_void() +
  theme(legend.position = "top") +
  scale_fill_manual(values = cores_estat, name = 'Turno')
ggsave("setor_turno_dos_jogos.pdf", width = 158, height = 93, units = "mm")

# Turno dos jogos em relação a fase ----
turno_jogo_fase <- turnos_jogos  %>%
  group_by(Stage,turno) %>%
  summarise(freq = n()) %>%
  mutate(
    freq_relativa = round(freq/sum(freq)*100,2)
  )

porcentagens <- str_c(turno_jogo_fase$freq_relativa, "%") %>% str_replace("\\.", ",")

legendas <- str_squish(str_c(turno_jogo_fase$freq," (", porcentagens, ")"))

ggplot(turno_jogo_fase) +
  aes(
    x = Stage, y = freq,
    fill = turno, label = legendas
  ) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = 0.5,
    size = 3
  ) +
  labs(x = "Fase", y = "Frequência", fill='Turno') +
  theme_estat()
ggsave("colunas-bi-freq-turn-fase.pdf", width = 158, height = 93, units = "mm")

# ANÁLISE 4 - Relação entre público total da copa e número de gols marcados

# Gol total por copas ----
ggplot(World_Cups) +
  aes(x=Year, y=GoalsScored, group=1) +
  geom_line(size=1,colour="#A11D21") + 
  geom_point(colour="#A11D21",size=2) +
  labs(x="Edição", y="Gols") +
  scale_y_continuous(breaks = seq(0,200,10))+
  scale_x_continuous(breaks = seq(1930,2014,4))+
  theme_estat(axis.text.x = element_text(angle=90))
ggsave("series_uni_gols_por_edicao.pdf", width = 158, height = 93, units = "mm")

# quntidade de gols ----
gols_public<-Jogos%>%
  mutate(gols=`Home Team Goals`+`Away Team Goals`)

gols_public$frequencia_gols<-cut(gols_public$gols,
                 breaks = seq(-1,12,1),
                 labels = c('0 Gols', '1 Gol', '2 Gols', '3 Gols', '4 Gols',
                            '5 Gols','6 Gols','7 Gols','8 Gols','9 Gols',
                            '10 Gols','11 Gols','12 Gols'))

gols_public$frequencia_gols<-gols_public$frequencia_gols%>%
  str_replace('10 Gols|11 Gols|12 Gols','Mais de 10')

contagem <- gols_public %>% 
  group_by(frequencia_gols) %>%
  summarise(Freq = n()) %>%
  mutate(Prop = round(100*(Freq/sum(Freq)), 2)) %>%
  arrange(desc(frequencia_gols)) 

porcentagens <- str_c(contagem$Prop, "%") %>% str_replace("\\.", ",")

legendas <- str_squish(str_c(contagem$Freq, " (", porcentagens, ")"))

ggplot(contagem) +
  aes(x = frequencia_gols, y = Freq, label = legendas) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  scale_y_continuous(limits = c(0,200))+
  #scale_x_continuous()+
  labs(x = "manufacturer", y = "Frequência") +
  theme_estat()
ggsave("colunas-total-de-gols-por-partida.pdf", width = 158, height = 93, units = "mm")


# Boxplot entre quantidade de gols e público 
gols_public<-Jogos%>%
  mutate(gols=`Home Team Goals`+`Away Team Goals`)

gols_public$frequencia_gols<-cut(gols_public$gols,
                                 breaks = seq(-1,12,1),
                                 labels = c('0 Gols', '1 Gol', '2 Gols', '3 Gols', '4 Gols',
                                            '5 Gols','6 Gols','7 Gols','8 Gols','9 Gols',
                                            '10 Gols','11 Gols','12 Gols'))

gols_public$frequencia_gols<-gols_public$frequencia_gols%>%
  str_replace('10 Gols|11 Gols|12 Gols','Mais de 10')%>%
  str_replace('1 Gol|2 Gols|3 Gols', '1 a 3 gols')%>%
  str_replace('4 Gols|5 Gols|6 Gols', '4 a 6 gols')%>%
  str_replace('7 Gols|8 Gols|9 Gols', '7 a 9 gols')

ggplot(gols_public) +
  aes(x = frequencia_gols, y = Attendance/1000) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
  ) +
  scale_y_continuous(limits = c(0,200))+
  labs(x = "Quantidade de gols na partida", y = "Público na partida (em milhares)") +
  theme_estat()
ggsave("box_bi_quant_gols_e_publi.pdf", width = 158, height = 93, units = "mm")


# Gráfico de dispersão sobre o número de gols em relação ao total de público
World_Cups['edicao']<-World_Cups$Year%>%
  str_replace('1930|1934|1938|1950','1930-1950')%>%
  str_replace('1954|1958|1962|1966|1970','1954-1970')%>%
  str_replace('1974|1978|1982|1986|1990','1974-1990')%>%
  str_replace('1994|1998|2002|2006|2010|2014','1990-2014')


ggplot(World_Cups) +
  aes(x = GoalsScored, y = Attendance/1000000, color=edicao) +
  geom_point(size = 3) +
  labs(
    x = "Total de Gols",
    y = "Total de público (em milhões)", color='Edições'
  ) +
  scale_y_continuous(breaks = seq(0,4,0.5), limits = c(0,4))+
  scale_x_continuous(breaks = seq(0,200,10),limits = c(65,180))+
  theme_estat()
ggsave("disp_multi_totgol_totpopu_edicao.pdf", width = 158, height = 93, units = "mm")


# ANÁLISE 5 - Top 10 países com maior soma de público de todas as copas

public_casa<-data.frame()
for (i in 1:836) {
  x<-subset(Jogos, Jogos$`Home Team Name`==Jogos$`Home Team Name`[i])
  x<-x%>%
    mutate(public_total = sum(x$Attendance))%>%
    mutate(selecao=x$`Home Team Name`)
  public_casa<-bind_rows(public_casa,x[1,])
}
public_casa<-public_casa%>%
  distinct(public_casa$MatchID, .keep_all = T)  

public_fora<-data.frame()
for (i in 1:836) {
  x<-subset(Jogos, Jogos$`Away Team Name`==Jogos$`Away Team Name`[i])
  x<-x%>%
    mutate(public_total = sum(x$Attendance))%>%
    mutate(selecao=x$`Away Team Name`)
  public_fora<-bind_rows(public_fora,x[1,])
}
public_fora<-public_fora%>%
  distinct(public_fora$MatchID, .keep_all = T)  

ranq_public<-bind_rows(public_casa[-23],public_fora[-23])

# Seleções com maior púbico fora de casa 

top10_fora<-public_fora[order(public_fora$public_total,decreasing = T),]
top10_fora<-top10_fora[1:10,]

ggplot(top10_fora) +
  aes(y = fct_reorder(selecao, round(public_total/1000000,2)), 
      x = round(public_total/1000000,2), 
      label = round(public_total/1000000,2)) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    hjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  labs(x = "Público (em milhões)", y = "Seleção") +
  scale_x_continuous(limits = c(0,2))+
  theme_estat()
ggsave("colunas-publ-fora.pdf", width = 158, height = 93, units = "mm")

# Seleções com maior público em casa 

top10_casa<-public_casa[order(public_casa$public_total,decreasing = T),]
top10_casa<-top10_casa[1:10,]

ggplot(top10_casa) +
  aes(y = fct_reorder(selecao, round(public_total/1000000,2)), 
      x = round(public_total/1000000,2), 
      label = round(public_total/1000000,2)) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    hjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  labs(x = "Público (em milhões)", y = "Seleção") +
  scale_x_continuous(limits = c(0,5))+
  theme_estat()
ggsave("colunas-publ-casa.pdf", width = 158, height = 93, units = "mm")

# Seleções com maior público

top10_ranq<-data.frame()
for (i in 1:161) {
  x<-subset(ranq_public, ranq_public$selecao==ranq_public$selecao[i])
  x<-x%>%
    mutate(pub_geral = sum(x$public_total))
  top10_ranq<-bind_rows(top10_ranq,x[1,])
}
top10_ranq<-top10_ranq%>%
  distinct(top10_ranq$selecao, .keep_all = T)  

top10_ranq<-top10_ranq[order(top10_ranq$pub_geral,decreasing = T),]
top10_ranq<-top10_ranq[1:10,]

ggplot(top10_ranq) +
  aes(y = fct_reorder(selecao, round(pub_geral/1000000,2)), 
      x = round(pub_geral/1000000,2), 
      label = round(pub_geral/1000000,2)) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    hjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  scale_x_continuous(limits = c(0,6))+
  labs(x = "Público (em milhões)", y = "Seleção") +
  theme_estat()
ggsave("colunas-publ-geral.pdf", width = 158, height = 93, units = "mm")


# ANÁLISE 6 

pontos_camp<-data.frame()
for (i in 1:20) {
  x<-subset(World_Cups[,1:6], World_Cups$Winner==World_Cups$Winner[i])
  x<-x%>%
    mutate(ponto=4,situacao='Campeão')%>%
    mutate(pontos=sum(ponto))%>%
    mutate(selecao=x$Winner)
  pontos_camp<-bind_rows(pontos_camp,x[1,])
}
pontos_camp<-pontos_camp[-1:-7]%>%
  distinct(pontos_camp$selecao, .keep_all = T)  

pontos_vice<-data.frame()
for (i in 1:20) {
  x<-subset(World_Cups[,1:6], World_Cups$`Runners-Up`==World_Cups$`Runners-Up`[i])
  x<-x%>%
    mutate(ponto=3,situacao='Vice-Campeão',pontos=sum(ponto),selecao=x$`Runners-Up`)
  pontos_vice<-bind_rows(pontos_vice,x[1,])
}
pontos_vice<-pontos_vice[-1:-7]%>%
  distinct(pontos_vice$selecao, .keep_all = T) 

pontos_ter<-data.frame()
for (i in 1:20) {
  x<-subset(World_Cups[,1:6], World_Cups$Third==World_Cups$Third[i])
  x<-x%>%
    mutate(ponto=2,situacao='Terceiro Lugar',pontos=sum(ponto),selecao=x$Third)
  pontos_ter<-bind_rows(pontos_ter,x[1,])
}
pontos_ter<-pontos_ter[-1:-7]%>%
  distinct(pontos_ter$selecao, .keep_all = T) 

pontos_quar<-data.frame()
for (i in 1:20) {
  x<-subset(World_Cups[,1:6], World_Cups$Fourth==World_Cups$Fourth[i])
  x<-x%>%
    mutate(ponto=1,situacao='Quarto Lugar',pontos=sum(ponto),selecao=x$Fourth)
  pontos_quar<-bind_rows(pontos_quar,x[1,])
}
pontos_quar<-pontos_quar[-1:-7]%>%
  distinct(pontos_quar$selecao, .keep_all = T) 

pontos_geral<-bind_rows(pontos_camp[-4],pontos_vice[-4],pontos_ter[-4],pontos_quar[-4])
pontos_geral$id<-c(1:46)
rank_pontos<-data.frame()
for (i in 1:20) {
  x<-subset(pontos_geral,pontos_geral$selecao==pontos_geral$selecao[i])
  x<-x%>%
    mutate(pontos_totais=sum(pontos))
  rank_pontos<-bind_rows(rank_pontos,x)
}
rank_pontos<-rank_pontos%>%
  distinct(rank_pontos$id, .keep_all = T) 

ggplot(rank_pontos%>%filter(!selecao%in%c('USA','England','Spain'))) +
  aes(y = fct_reorder(selecao, pontos_totais), 
      x = pontos, 
      fill=situacao, label='') +
  geom_bar(stat = "identity", position = 'stack', width = 0.7)+
  geom_text(
    position = position_dodge(width = .9),
    hjust = -0.5, #hjust = .5,
    size = 3
  )+
  scale_x_continuous(breaks = seq(0,50,5))+
  theme_estat()+
  labs(x = "Pontos", y = "Seleção", fill='Situação') 
ggsave("colunas-rank_pontos-divididos.pdf", width = 158, height = 93, units = "mm")

pontos_geral<-bind_rows(pontos_camp[-4],pontos_vice[-4],pontos_ter[-4],pontos_quar[-4])
pontos_geral$id<-c(1:46)
rank_pontos<-data.frame()
for (i in 1:20) {
  x<-subset(pontos_geral,pontos_geral$selecao==pontos_geral$selecao[i])
  x<-x%>%
    mutate(pontos_totais=sum(pontos))
  rank_pontos<-bind_rows(rank_pontos,x)
}
rank_pontos<-rank_pontos%>%
  distinct(rank_pontos$selecao, .keep_all = T) 

top10_ranq<-rank_pontos[order(rank_pontos$pontos_totais,decreasing = T),]
top10_ranq<-top10_ranq[1:10,]

ggplot(top10_ranq) +
  aes(y = fct_reorder(selecao, pontos_totais), 
      x = pontos_totais, label=pontos_totais) +
  geom_bar(stat = "identity", width = 0.7, fill='#A11D21')+
  geom_text(
    position = position_dodge(width = .9),
    hjust = -0.5, #hjust = .5,
    size = 3
  )+
  scale_x_continuous(breaks = seq(0,50,5))+
  theme_estat()+
  labs(x = "Pontos", y = "Seleção") 
ggsave("colunas-rank_pontos.pdf", width = 158, height = 93, units = "mm")
